/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author admin
 */
public class ObjectTest {
    
    public ObjectTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

     /**
     * Test of Construction of class Card.
     */
    
    
        @Test
    public void testConstructionFailsIfAdressIsNull() throws MyException {
        System.out.println("testConstructionFailsIfAdressIsNull");
        try {
            new Object(null,"desc" , "type") ;
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
    @Test
    public void testConstructionFailsIfAdressIsEmpty() throws MyException {
        System.out.println("ConstructionFailsIfAdressIsEmpty");
        try {
            new Object("","desc" , "type") ;
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
        @Test
    public void testConstructionFailsIfTypeIsNull() throws MyException {
        System.out.println("ConstructionFailsIfTypeIsNull");
        try {
            new Object("address","desc" , null) ;
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
        @Test
    public void testConstructionFailsIfTypeIsEmpty() throws MyException {
        System.out.println("ConstructionFailsIfTypeIsEmpty");
        try {
            new Object("address","desc" , "") ;
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
        @Test       
    public void testSet_param_addressISnull() throws MyException {
        System.out.println("testSet_param_addressISnull");
        try {
            Object ob = new Object("address","desc" , "type") ;
            
            ob.setParam(null,"desc" , "type");
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
        @Test
    public void testSet_param_addressISempty() throws MyException {
        System.out.println("testSet_param_addressISempty");
        try {
            Object ob = new Object("address","desc","type") ;
            ob.setParam("","desc" , "type");
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
        @Test
    public void testSet_param_typeISnull() throws MyException {
        System.out.println("testSet_param_typeISnull");
        try {
            String Specific = null;
            Object ob = new Object("address","desc" , "type") ;
            ob.setParam("address","desc", Specific);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
        @Test
    public void testSet_param_typeISempty() throws MyException {
        System.out.println("testSet_param_typeISempty");
        try {
            Object ob = new Object("address","desc" , "type") ;
            
            ob.setParam("address","desc" , "");
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
        @Test
    public void testGetAdress() throws MyException{
       System.out.println("testGetAdress");
       Object ob = new Object("address","desc" , "type");
       String address = "vbab";
       ob.setParam(address,"desc" , "type");
       ob.getAddress();
       assertEquals(address, ob.getAddress());
    }
    
        @Test
    public void testGetType() throws MyException{
       System.out.println("testGetType");
       Object ob = new Object("address","desc" , "type");
       String type = "vbab";
       ob.setParam(type,"desc" , "type");
       ob.getType();
       assertEquals(type, ob.getAddress());
    }
    
        @Test
    public void testgetDescription() throws MyException{
       System.out.println("testgetDescription");
       Object ob = new Object("address","desc","type");
       String dis = "vbab";
       ob.setParam("type", dis, "type");
       ob.getDescription();
       assertEquals(dis, ob.getDescription());
    }
}
