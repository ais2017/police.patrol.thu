/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class CallForHelpTest {
    
    public CallForHelpTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

     /**
     * Test of Construction of class Card.
     */
    
        @Test
    public void testConstructionFailsIf_Dop_brIsNegative() throws MyException {
        System.out.println("ConstructionFailsIf_Dop_br_IsNegative");
        try {
            CallForHelp call = new CallForHelp();
            call.CallForHelp_dop_br(-10);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
       
        @Test
    public void testConstructionFailsIf_Emergency_IsNegative() throws MyException {
        System.out.println("ConstructionFailsIf_Emergency_IsNegative");
        try {
            CallForHelp call = new CallForHelp();
            call.CallForHelp_Emergency(-10);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
        @Test
    public void testConstructionFailsIf_Specific_IsNull() throws MyException {
        System.out.println("ConstructionFailsIf_Specific_IsNull");
        try {
            CallForHelp call = new CallForHelp();
            call.CallForHelp_Specific(null);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
            @Test
    public void testConstructionFailsIf_Specific_IsEmpty() throws MyException {
        System.out.println("ConstructionFailsIf_Specific_IsEmpty");
        try {
            CallForHelp call = new CallForHelp();
            call.CallForHelp_Specific("");
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
             @Test
    public void testConstructionFailsIf_Status_IsAlredyCLOSED() throws MyException {
        System.out.println("ConstructionFailsIf_Status_IsAlredyCLOSED");
        try {
            CallForHelp call = new CallForHelp();
            call.setStatus("closed");
            call.setStatus("closed");
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
                 @Test
    public void testConstructionFailsIf_Status_IsNull() throws MyException {
        System.out.println("ConstructionFailsIf_Status_IsNull");
        try {
            CallForHelp call = new CallForHelp();
            call.setStatus(null);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
        @Test
    public void testConstructionFailsIf_Status_IsEmpty() throws MyException {
        System.out.println("ConstructionFailsIf_Status_IsEmpty");
        try {
            CallForHelp call = new CallForHelp();
            call.setStatus("");
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
           @Test
    public void testConstructionFailsIf_statusIsNotClosed_try_get_End_date() throws MyException {
        System.out.println("ConstructionIf_statusIsNotClosed_try_get_End_date");
        try {
            CallForHelp call = new CallForHelp();
            call.getEnd_date();
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
   
        @Test
    public void testGetStatus() throws MyException{
       System.out.println("testGetStatus");
       CallForHelp call = new CallForHelp();
       String status = "closed";
       call.setStatus(status);
       assertEquals(status, call.getStatus());
    }

           @Test
    public void testGetSpecific() throws MyException{
       System.out.println("testGetSpecific");
       CallForHelp call = new CallForHelp();
       String Specific = "захват заложников";
       call.CallForHelp_Specific(Specific);
       assertEquals(Specific, call.getSpecific());
    }
    
           @Test
    public void testGetEmergence() throws MyException{
       System.out.println("testGetEmergence");
       CallForHelp call = new CallForHelp();
       int Emergence = 2;
       call.CallForHelp_Emergency(Emergence);
       assertEquals(Emergence, call.getEmergence());
    }
           @Test
    public void testGetDop_br() throws MyException{
       System.out.println("testGetDop_br");
       CallForHelp call = new CallForHelp();
       int dop = 5;
       call.CallForHelp_dop_br(dop);
       assertEquals(dop, call.getDop_br());
    }
    
}
