/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author admin
 */
public class ReportTest {
    
    public ReportTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

     /**
     * Test of Construction of class Card.
     */
        @Test
    public void testConstructionFailsIfContentIsNull() throws MyException {
        System.out.println("ConstructionFailsIfContentIsNull");
        try {
            new Report(null);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
        @Test
    public void testConstructionFailsIfContentIsEmpty() throws MyException {
        System.out.println("ConstructionFailsIfIdIsEmpty");
        try {
            new Report("");
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
   
        @Test
    public void testgetContent() throws MyException{
       System.out.println("testgetContent");
       String content = "захват заложников";
       Report report = new Report(content);
       assertEquals(content, report.getContent());
    }

    
}
