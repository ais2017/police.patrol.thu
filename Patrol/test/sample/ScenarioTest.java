package sample;

import java.math.BigDecimal;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author admin
 */
public class ScenarioTest {
    Scenario scenario;
    Brigade br;
    Task task;
    Object ob;
    
    public ScenarioTest() {
    }
    
    @Before
    public void setUp() throws Exception{
       BrigadesImpl brs = new BrigadesImpl();
       TasksImpl tasks = new TasksImpl();
       ObjectsImpl obj = new ObjectsImpl();
       scenario = new Scenario(brs, tasks, obj);
       scenario.brigadesImpl.CreateBrigadeList();
       scenario.objectsImpl.CreateObjectList();
     /*  Long k = 1l;  
       scenario.CreateBrigade(k);
       Long k = 1l;
       Long k2 = 2l;
       Card addCard = new Card("1234 5678 7777 2378", 500);
       scenario.AddTask_with_object_ToBrigade(k,"5 416470 090014",1);
       Purchase puch = scenario.purchasesImpl.getById(k);
       puch.closePurchase();
       scenario.purchasesImpl.UpdatePurchase(puch, k);
       scenario.CreatePurchase("2 670500 090014",7);
       Purchase puch2 = scenario.purchasesImpl.getById(k2);
       puch2.setCard(addCard);
       scenario.purchasesImpl.UpdatePurchase(puch2, k2);
       scenario.CreatePurchase("8 410545 090014",10);
       scenario.CreateReturn(k,"7 209380 644345",3);*/
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of Create method, of class Scenario.
     */
    @Test
    public void testCreateBrigade1() throws Exception {
        System.out.println("testCreateBrigade1");
        try {
           scenario.CreateBrigade(1l);
           Assert.fail("Expected MyException");
        }
             catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
      
      /*      @Test
    public void testCreateBrigade2() throws Exception {
        System.out.println("NEEDFAILtestCreateBrigade2");
        try {
            Long s = 33321212l;
           scenario.CreateBrigade(s);
           Assert.fail("Expected MyException");
        }
             catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    } */
    
        @Test
    public void testAddTask_with_object_ToBrigade_Brigade_dont_exist() throws Exception {
        System.out.println("testAddTask_with_object_ToBrigade_Brigade_dont_exist");
        try {
           
           scenario.AddTask_with_object_ToBrigade("3232",222222l,"dsd","dsds",22l,2);
           Assert.fail("Expected MyException");
        }
             catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
            @Test
    public void testAddTask_with_object_ToBrigade_Task_already_exist() throws Exception {
        System.out.println("testAddTask_with_object_ToBrigade_Task_already_exist");
        try {
           scenario.CreateBrigade(222222l);
           scenario.CreateBrigade(211222l);
           scenario.AddTask_with_object_ToBrigade("3232",211222l,"dsd","dsds",22l,2);
           scenario.AddTask_with_object_ToBrigade("3232",222222l,"dsd","dsds",22l,2);
           Assert.fail("Expected MyException");
        }
             catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
                @Test
    public void testAddTask_with_object_ToBrigade_Object_dont_create() throws Exception {
        System.out.println("testAddTask_with_object_ToBrigade_Object_dont_create");
        try {
            
           scenario.CreateBrigade(222222l);
           scenario.AddTask_with_object_ToBrigade("3232",222222l,"dsd","dsds",7l,2);
         //  scenario.addCall_DopBr(1l,"3232", 0);
       //    scenario.AddTask_with_object_ToBrigade("3232",1l,"dsd","dsds",22l,2);
       //    scenario.AddTask_with_object_ToBrigade("3232",222222l,"dsd","dsds",22l,2);
           Assert.fail("Expected MyException");
        }
             catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
    @Test
        public void testaddCall_DopBr_no_Tasks() throws Exception {
        System.out.println("testaddCall_DopBr_no_Tasks");
        try {
           scenario.CreateBrigade(222222l);
           scenario.addCall_DopBr(222222l,"dsaaads",7);
           Assert.fail("Expected MyException");
        }
             catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }

    }
    
    
    

