/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import java.util.*;
import java.text.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author admin
 */
public class TaskTest {
    
    public TaskTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

     /**
     * Test of Construction of class Card.
     */
    
 

    
    //TASK
    
        @Test
    public void testConstructionFailsIf_Address_IsNull() throws MyException {
        System.out.println("testConstructionFailsIf_Address_IsNull");
        try {
            String Specific = "sss";
            String Address = null;
            int Task_code = 10;  
            Task task = new Task("dsd",Specific,Address ,Task_code);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
  
    @Test
    public void testConstructionFailsIf_Address_IsEMPTY() throws MyException {
        System.out.println("testConstructionFailsIf_Address_IsEMPTY");
        try {
            String Specific ="sss";
            String Address ="";
            int Task_code = 10;  
            Task task = new Task("dsd",Specific,Address ,Task_code);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
   
     @Test
    public void testConstructionFailsIf_Specific_IsNull() throws MyException {
        System.out.println("testConstructionFailsIf_Specific_IsNull");
        try {
            String Specific = null;
            String Address = "ddd";
            int Task_code = 10;  
            Task task = new Task("dsd",Specific,Address ,Task_code);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
      
        @Test
    public void testConstructionFailsIf_Specific_IsEMPTY() throws MyException {
        System.out.println("testConstructionFailsIf_Specific_IsEMPTY");
        try {
            String Specific = "";
            String Address = "dddd";
            int Task_code = 10;  
            Task task = new Task("dsd",Specific,Address ,Task_code);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    } 
    
        @Test
    public void testConstructionFailsIfTask_codeIsNegative() throws MyException {
        System.out.println("ConstructionFailsIf_Task_code_IsNegative");
        try {
            String Specific = "ddeed";
            String Address = "dddd";
            int Task_code = -10;  
            Task task = new Task("dsd",Specific,Address ,Task_code);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
    //ENDTASK
    
    //addObject
        @Test
    public void test_addObject_already_add() throws MyException {
        System.out.println("test_addObject_already_add");
        try {
            String a = "ddddd";
            Object obi = new Object(a,a,a);
            int Task_code = 10;  
            Task task = new Task("dsd",a,a,obi,Task_code);       
            task.addObject(obi);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
    //removeObject
            @Test
    public void test_removeObject_EMPTY_collection() throws MyException {
        System.out.println("test_removeObject_EMPTY_collection");
        try {
            String a = "ddddd";
            Object obi = new Object(a,a,a);
              int Task_code = 10;  
            Task task = new Task("dsd",a,a,obi,Task_code);       
            task.removeObject(obi);
            task.removeObject(obi);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
        @Test
    public void testFailsIf_getEnd_time_without_closedStatysOfTask() throws MyException {
        System.out.println("testFailsIf_getEnd_time_without_closedStatysOfTask");
        try {
            String Specific = "ddeed";
            String Address = "dddd";
            int Task_code = 10;  
            Task task = new Task("dsd",Specific,Address ,Task_code);
            task.getEnd_date();
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
        @Test
    public void testFailsIf_Status_IsAlredyCLOSED() throws MyException {
        System.out.println("testFailsIf_Status_IsAlredyCLOSED");
        try {
            String Specific = "ddeed";
            String Address = "dddd";
            int Task_code = 10;  
            Task task = new Task("dsd",Specific,Address ,Task_code);
            task.setStatus("closed");
            task.setStatus("closed");
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
        @Test
    public void testFailsIf_Status_IsNull() throws MyException {
        System.out.println("testFailsIf_Status_IsNull");
        try {
             String Specific = "ddeed";
            String Address = "dddd";
            int Task_code = 10;  
            Task task = new Task("dsd",Specific,Address ,Task_code);
            task.setStatus(null);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
        @Test
    public void testFailsIf_Status_IsEmpty() throws MyException {
        System.out.println("testFailsIf_Status_IsEmpty");
        try {
            String Specific = "ddeed";
            String Address = "dddd";
            int Task_code = 10;  
            Task task = new Task("dsd",Specific,Address ,Task_code);
            task.setStatus("");
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
        //setSpecific
            @Test
    public void testFailsIf_Specific_IsNull() throws MyException {
        System.out.println("testFailsIf_Specific_IsNull");
        try {
             String Specific = "ddd";
            String Address = "dddd";
            int Task_code = 10;  
            Task task = new Task("dsd",Specific,Address ,Task_code);
            task.setSpecific(null);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
        @Test
    public void testFailsIf_Specific_IsEmpty() throws MyException {
        System.out.println("testFailsIf_Specific_IsEmpty");
        try {
            String Specific = "ddeed";
            String Address = "dddd";
            int Task_code = 10;  
            Task task = new Task("dsd",Specific,Address ,Task_code);
            task.setSpecific("");
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }    
    
      ////setAddress
            @Test
    public void testFailsIf_Address_IsNull() throws MyException {
        System.out.println("testFailsIf_Address_IsNull");
        try {
             String Specific = "ddd";
            String Address = "dddd";
            int Task_code = 10;  
            Task task = new Task("dsd",Specific,Address ,Task_code);
            task.setAddress(null);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
        @Test
    public void testFailsIf_Address_IsEmpty() throws MyException {
        System.out.println("testFailsIf_Address_IsEmpty");
        try {
            String Specific = "ddeed";
            String Address = "dddd";
            int Task_code = 10;  
            Task task = new Task("dsd",Specific,Address ,Task_code);
            task.setAddress("");
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }    
    
    //setTask_code
            @Test
    public void testFailsIf_setTask_code_IsNegative() throws MyException {
        System.out.println("testFailsIf_setTask_code_IsNegative");
        try {
            String Specific = "ddeed";
            String Address = "dddd";
            int Task_code = 10;  
            Task task = new Task("dsd",Specific,Address ,Task_code);
            task.setTask_code(-10);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
    //getEnd_date
        @Test
    public void testFailsIf_statusIsNotClosed_try_get_End_date() throws MyException {
        System.out.println("testFailsIf_statusIsNotClosed_try_get_End_date");
        try {
            String Specific = "ddeed";
            String Address = "dddd";
            int Task_code = 10;  
            Task task = new Task("dsd",Specific,Address ,Task_code);
            task.getEnd_date();
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
    //addReport
         @Test
    public void testGetReports() throws MyException{
       System.out.println("testGetReports");
       String Specific = "ddeed";
       String Address = "dddd";
       int Task_code = 10;  
       Task task = new Task("dsd",Specific,Address ,Task_code);
       task.addReport(Address);
       Collection per = task.getReports();
       Collection found = new ArrayList<Task>();
        Report check = new Report("qq");
        for (Iterator it = per.iterator(); it.hasNext();) {
            check = (Report)it.next();
        }
        assertEquals(Address, check.getContent());
    }
   
}