/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author admin
 */
public class BrigadeTest {
    
    public BrigadeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

     /**
     * Test of Construction of class Card.
     */
            @Test
    public void testConstructionFailsIf_numbers_IsNegative() throws MyException {
        System.out.println("testConstructionFailsIf_numbers_IsNegative");
        try {
            MapPoint map = new MapPoint(22f,44f);
            Brigade br = new Brigade(-5l);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
         @Test
    public void testGetNumber() throws MyException{
       System.out.println("testGetNumber");
       MapPoint map = new MapPoint(22f,44f);
       Long dop = 5l;
       Brigade br = new Brigade(dop);
       assertEquals(dop, br.getNumber());
    }
    
        @Test
    public void testFail_getTasks_sIfStatusIsNull() throws MyException {
        System.out.println("testFail_getTasks_sIfStatusIsNull");
        try {
            Float d = 11.6F;
            Float s = 11.6F;
            MapPoint map = new MapPoint(d,s);
            Brigade br = new Brigade(5l);
            br.getTasks(null);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
}
