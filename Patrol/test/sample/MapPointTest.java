
package sample;


import java.util.*;
import java.text.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class MapPointTest {
    
    public MapPointTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

     /**
     * Test of Construction of class Card.
     */
        @Test
    public void testConstructionFailsIfwrong_param_1() throws MyException {
        System.out.println("testConstructionFailsIfwrong_param_1");
        try {
            Float a =11.4f;
            Float b =-44.4f;
            MapPoint ma = new MapPoint(a,b);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }


           @Test
    public void testConstructionFailsIfwrong_param_2() throws MyException {
        System.out.println("testConstructionFailsIfwrong_param_2");
        try {
            Float a =9991.4f;
            Float b =11.4f;
            MapPoint ma = new MapPoint(a,b);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
               @Test
    public void testConstructionFailsIfwrong_param_3() throws MyException {
        System.out.println("testConstructionFailsIfwrong_param_3");
        try {
            Float a =11.4f;
            Float b =66611.4f;
            MapPoint ma = new MapPoint(a,b);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
               @Test
    public void testConstructionFailsIfwrong_param_4() throws MyException {
        System.out.println("testConstructionFailsIfwrong_param_4");
        try {
            Float a =-11.4f;
            Float b =11.4f;
            MapPoint ma = new MapPoint(a,b);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
            @Test
    public void test_set_wrong_param_1() throws MyException {
        System.out.println("test_set_wrong_param_1");
        try {
            Float a =11.4f;
            Float b =44.4f;
            MapPoint ma = new MapPoint(a,b);
            ma.setMapPoints(-1111f,11f);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }


           @Test
    public void test_set_wrong_param_2() throws MyException {
        System.out.println("test_set_wrong_param_2");
        try {
            Float a =91.4f;
            Float b =11.4f;
            MapPoint ma = new MapPoint(a,b);
            ma.setMapPoints(1111f,11f);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
               @Test
    public void test_set_wrong_param_3() throws MyException {
        System.out.println("test_set_wrong_param_3");
        try {
            Float a =11.4f;
            Float b =66611.4f;
            MapPoint ma = new MapPoint(a,b);
            ma.setMapPoints(11f,1111f);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
               @Test
    public void test_set_wrong_param_4() throws MyException {
        System.out.println("test_set_wrong_param_4");
        try {
            Float a =11.4f;
            Float b =11.4f;
            MapPoint ma = new MapPoint(a,b);
            ma.setMapPoints(11f,-1111f);
            Assert.fail("Expected MyException");
        }
        catch (MyException ex){
            Assert.assertNotEquals("", ex.getMessage());
        }
    }
    
    
            @Test
    public void testgetLat() throws MyException{
       System.out.println("testgetLat");
       Float a = 10f;
        MapPoint ma = new MapPoint(a,a);
       assertEquals(a, ma.getLattitude());
    }
    
            @Test
    public void testgetLong() throws MyException{
       System.out.println("testgetLong");
       Float a = 10f;
       MapPoint ma = new MapPoint(a,a);
       assertEquals(a, ma.getLongtitude());
    }
}
    
    