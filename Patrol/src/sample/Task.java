/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;
import java.util.*;
import java.text.*;

/**
 *
 * @author katerina
 */

public class Task {
        private  String id;
        private  String status;
	private  Date start_date;
	private  Date end_date;
	private  String specific;
        private  String address;
	private  int task_code;
        Collection reports;
        Collection calls;
        Collection objects;
    
     //tasr_with address  
    public Task(String Id,String Specific,String Address ,int Task_code) throws MyException{
       if ((Address == null) || (Address == ""))
       {
           throw new MyException("Адрес не может быть пустым!");
       } 
	    if ((Specific == null) || (Specific == ""))
       {
           throw new MyException("Поле детали не может быть пустыми!");
       } 
	    if (Task_code <= 0)
       {
           throw new MyException("Код вызова должен быть больше нуля!");
       }
            if ((Id == null) || (Id ==""))
       {
           throw new MyException("Id Поле не может быть пустыми!");
       }
       this.id = Id;
       List<CallForHelp> call = new ArrayList<CallForHelp>(1);
       this.calls = call;
       this.task_code = task_code;
       List<Report> report = new ArrayList<Report>(1);
       this.reports = report;
       reports.clear();
       List<Object> object = new ArrayList<Object>(1);
       this.objects = object;
       
       this.address = Address;
       this.specific = Specific;
       this.task_code = Task_code;
    }
	
    
    //task_with_objects
    public Task(String Id,String Specific,String Address ,Collection object ,int Task_code) throws MyException{
       
       if ((Address == null) || (Address == ""))
       {
           throw new MyException("Адрес не может быть пустым!");
       } 
	    if ((Specific == null) || (Specific == ""))
       {
           throw new MyException("Поле детали не может быть пустыми!");
       } 
	    if ( object == null )
       {
           throw new MyException("object не может быть пустыми!");
       }
                   if ((Id == null) || (Id ==""))
       {
           throw new MyException("Id Поле не может быть пустыми!");
       }
       this.id = Id;
       this.task_code = task_code;
       List<CallForHelp> call = new ArrayList<CallForHelp>(1);
       this.calls = call;   
       List<Report> report = new ArrayList<Report>(1);
       this.reports = report;
       reports.clear();
       this.address = Address;
       this.specific = Specific;
       this.objects = object;
       
    }
	
    //task_with_object
    public Task(String Id,String Specific,String Address ,Object object, int Task_code) throws MyException{
       
       if ((Address == null) || (Address == ""))
       {
           throw new MyException("Адрес не может быть пустым!");
       } 
        if ((Specific == null) || (Specific == ""))
       {
           throw new MyException("Поле детали не может быть пустыми!");
       } 
        if ( object == null )
       {
           throw new MyException("object не может быть пустыми!");
       }  
            
       if ((Id == null) || (Id == ""))
       {
           throw new MyException("Id Поле не может быть пустыми!");
       }
       this.id = Id;
       this.task_code = task_code;
       List<CallForHelp> call = new ArrayList<CallForHelp>(1);
       this.calls = call;   
       calls.clear();
       List<Report> report = new ArrayList<Report>(1);
       this.reports = report;
       reports.clear();
       this.address = Address;
       this.specific = Specific;
       List<Object> orders = new ArrayList<Object>(1);
       orders.add(object);
       this.objects = orders;
     }
	
	
    public void addReport(String Content) throws MyException {
        Report report1 = new Report(Content);
        if (reports.isEmpty()){
            List<Report> report = new ArrayList<Report>(1);
            report.add(report1);
            this.reports = report;
        }
	reports.add(report1);
    }
    

    public CallForHelp addCallForHelp_db(int n_dop) throws MyException {    
             CallForHelp call = new CallForHelp();
             call.CallForHelp_dop_br(n_dop);
        if (calls.isEmpty()){
                 List<CallForHelp> call1 = new ArrayList<CallForHelp>(1);
                 call1.add(call);
                 this.calls = call1;
        }
		calls.add(call);
                return call;
    }
	
    public CallForHelp addCallForHelp_emergency(int num) throws MyException {    
		CallForHelp call = new CallForHelp();
		call.CallForHelp_Emergency(num);
        if (calls.isEmpty()){
                     List<CallForHelp> call1 = new ArrayList<CallForHelp>(1);
                     call1.add(call);
                     this.calls = call1;
        }   
		calls.add(call);
                return call;
    }
	
    public CallForHelp addCallForHelp_Specific(String info) throws MyException {    
		CallForHelp call = new CallForHelp();
		call.CallForHelp_Specific(info);
                 if (calls.isEmpty()){
                    List<CallForHelp> call1 = new ArrayList<CallForHelp>(1);
                    call1.add(call);
                    this.calls = call1;
                }
		calls.add(call);
                return call;
    }
	
    public void addObject(Object addobject) throws MyException{
        if (objects.isEmpty()) {
           List<Object> orders = new ArrayList<Object>(1);
           orders.add(addobject);
           this.objects = orders;
        }
        else {
             if ( (objects.contains(addobject)) == true)
           {
              throw new MyException("Этот объект уже добавлен");
            }
        objects.add(addobject);
        }
}

    public void removeObject(Object object) throws MyException{ 
        if (objects.isEmpty()) {
              throw new MyException("Невозможно выполнить операцию!Объекты уже пусты");
        }
        objects.remove(object);
    }
	
    public void setStatus(String Status) throws MyException{
        if (this.status == "closed" )
        {
           throw new MyException("Невозможно выполнить операцию! Статус заявки уже закрыт");
        } 
		
		if ((Status == null) || (Status == ""))
        {
           throw new MyException("Невозможно выполнить операцию! Новый стату не может быть пустым");
        } 
		
		if (Status == "closed" )
        {
           this.end_date= new Date();
        } 
        this.status = Status;
	}
	
    public void setSpecific(String Specific) throws MyException{
   		if ((Specific == null) || (Specific == ""))
        {
           throw new MyException("Нельзя заменить строку детали на пустую");
        } 
        this.specific = Specific;
	}
	
    public void setAddress(String Address) throws MyException{
   		if ((Address == null) || (Address == ""))
        {
           throw new MyException("Нельзя заменить строку Address на пустую");
        } 
        this.address = Address;
	}
	
    public void setTask_code(int Task_code) throws MyException{
 	if(Task_code <= 0)
       {
           throw new MyException("Код вызова должен быть больше нуля!");
       }
        this.task_code = Task_code;
	}

	
    public String getStatus() {
       return this.status;
    } 
    
    public int getTask_code() {
       return this.task_code;
    } 
     public String getId() {
       return this.id;
    } 
 
    public String getStart_date() {
       SimpleDateFormat formatForDateNow = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
       String rDate = formatForDateNow.format(this.start_date);
       return rDate;
    }
	
    public String getEnd_date() throws MyException{
       
       if (this.status != "closed")
       {
           throw new MyException("На вызов о помощи еще не отреагировали!");
       }
		
       SimpleDateFormat formatForDateNow = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
       String rDate = formatForDateNow.format(this.end_date);
       return rDate;
    } 
    
    public Collection getReports() {
        return reports;
    }
	
    public Collection getCalls() {
        return calls;
    }
	
    public Collection getObjects() {
        return objects;
    }	
       
}
