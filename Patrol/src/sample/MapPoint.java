/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author katerina
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;
import java.util.*;
import java.text.*;


public class MapPoint {
    private  Float longtitude;
    private  Float lattitude;
    public MapPoint(Float Longtitude, Float Lattitude)throws MyException {
               if (Lattitude > 360f || Lattitude < 0f || Longtitude > 360f || Longtitude < 0f )
               {
            throw new MyException("Invalid params for Brigade class");
             }
       this.longtitude = Longtitude;
       this.lattitude = Lattitude;
    }
    
    public void setMapPoints(Float Longtitude, Float Lattitude) throws MyException {
               if (Lattitude > 360f || Lattitude < 0f || Longtitude > 360f || Longtitude < 0f ){
            throw new MyException("Invalid params for Brigade class");
               }
       this.longtitude = Longtitude;
       this.lattitude = Lattitude;
    } 
	
	public Float getLongtitude() {
       return this.longtitude;
    } 
	 public Float getLattitude() {
       return this.lattitude;
    } 

       
}
