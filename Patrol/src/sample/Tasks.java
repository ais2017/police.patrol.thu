/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import java.util.ArrayList;

/**
 *
 * @author admin
 */
public interface Tasks {
    
    public void save(Task task);

    public ArrayList<Task> ShowAllTasks();

    public Task getById(String id);
    
    void UpdateTask (Task newtask) throws Exception;
}




