/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import java.util.*;

public class BrigadesImpl implements Brigades {
    public Map<Long, Brigade> brigades;
    
    public BrigadesImpl (){
        this.brigades=new HashMap<>();
    }

    @Override
    public void CreateBrigadeList() throws Exception{
        Brigade card1 = new Brigade(1l);
        Brigade card2 = new Brigade(12l);
        Brigade card3 = new Brigade(123l);
        Brigade card4 = new Brigade(2985l);
        Brigade card5 = new Brigade(1234l);
        brigades.put(card1.getNumber(), card1);
        brigades.put(card2.getNumber(), card2);
        brigades.put(card3.getNumber(), card3);
        brigades.put(card4.getNumber(), card4);
        brigades.put(card5.getNumber(), card5);
    }

    @Override
    public void save(Brigade bra) {
        brigades.put(bra.getNumber(), bra);
    }
    @Override
    public Brigade getById(Long id) {
        return brigades.get(id);
    }

    @Override
    public ArrayList<Brigade> ShowAllBrigades () {
        ArrayList ListOfBrigades = new ArrayList<>(brigades.entrySet());
        return ListOfBrigades;
    }
    
    @Override
     public void UpdateBrigade (Brigade newCard) throws Exception {
        if (getById(newCard.getNumber())!=null) {
            brigades.put(newCard.getNumber(), newCard);
        }
        else
            throw new Exception();
    }

}
