/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;
/**
 *
 * @author katerina
 */



import java.util.*;
import java.math.BigDecimal;

public class Brigade {
    private Long number;
    private Collection maps;
    private Collection tasks;
    public Brigade(Long number) throws MyException {
        if ( number < 0 )
            throw new MyException("Invalid params for Brigade class");
      
       List<MapPoint> map = new ArrayList<MapPoint>(1);
       this.maps = map;
       maps.clear();
       
       List<Task> task = new ArrayList<Task>(1);
       this.tasks = task;
       tasks.clear();
       
       this.number = number;
    }


    public Task add_task_with_address(String Id,String Specific,String Address ,int Task_code) throws MyException{           
	   Task addtask = new Task(Id,Specific,Address,Task_code);
	   tasks.add(addtask);
           return addtask;
    }
	
    public Task add_task_with_object(String Id,String Specific,String Address ,Object object,int Task_code) throws MyException{
	   Task addtask = new Task(Id,Specific,Address,object,Task_code);
	   tasks.add(addtask);
           return addtask;
    }
	
	public Task add_task_with_objects(String Id,String Specific,String Address ,Collection object,int Task_code) throws MyException{
	   Task addtask = new Task(Id,Specific,Address,object,Task_code);
	   tasks.add(addtask);
           return addtask;
    }
     
    public void setMap(Float Longtitude, Float Lattitude) throws MyException {
        MapPoint map = new MapPoint(Longtitude,Lattitude);
        maps.add(map);
    }    
        
    public Long getNumber() {
        return this.number;
    }

    public Collection getallMap() {
        return maps;
    }
    
    public Collection getTasks(String Status) throws MyException{
       if ((Status == null) || (Status == ""))
       {
           throw new MyException("Status не может быть пустым!");
       } 
            Collection found = new ArrayList<Task>();
	    Task check = new Task("SS","a","a",1);
        for (Iterator it = tasks.iterator(); it.hasNext();) {
            check = (Task)it.next();
            if (check.getStatus() == Status )
                found.add(check);
        }
	return found;
    }
    
    public void removeTask(Task task) throws MyException{ 
        if (tasks.isEmpty()) {
              throw new MyException("Невозможно выполнить операцию!Объекты уже пусты");
        }
        tasks.remove(task);
    }
    
    public void addTask(Task addtask) throws MyException{ 
        if ( tasks.contains(addtask)) {
              throw new MyException("Невозможно выполнить операцию!");
        }
        tasks.add(addtask);
    }
    
 
}