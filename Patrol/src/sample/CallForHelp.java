/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;
import java.util.*;
import java.text.*;
/**
 *
 * @author katerina
 */

public class CallForHelp {
    private  String status;
    private  int dop_br;
	private  int emergency;
	private  String specific;
	private  Date start_date;
	private  Date end_date;
    public void CallForHelp(){
       this.status="new";
       this.start_date=new Date();
    }    
     
        
    public void CallForHelp_dop_br(int Dop_br) throws MyException{
       
       if (Dop_br <= 0)
       {
           throw new MyException("Число дополнительных бригад должно быть больше нуля!");
       }
        
       this.dop_br = Dop_br;
       this.emergency = 0;
    }
	
    public void CallForHelp_Emergency(int Emergency) throws MyException{
       
       if (Emergency <= 0)
       {
           throw new MyException("Номер экстренной службы должен быть больше нуля!");
       }
        
       this.emergency = Emergency;
    }
    
    public void CallForHelp_Specific(String Specific) throws MyException{
       
      if ((Specific == null) || (Specific == ""))
       {
           throw new MyException("Детали о вызове не могут быть пустыми!");
       } 
     
       this.specific = Specific;
       this.emergency =-1;

    }
    
    public void setStatus(String Status) throws MyException{
        if (this.status == "closed" )
        {
           throw new MyException("Невозможно выполнить операцию! Статус заявки уже закрыт");
        } 
		
		if ((Status == null) || (Status == ""))
        {
           throw new MyException("Невозможно выполнить операцию! Новый стату не может быть пустым");
        } 
		
		if (Status == "closed" )
        {
           this.end_date= new Date();
        } 
        this.status = Status;
	}
	
    public String getStatus() {
       return this.status;
    } 
	
    public int getDop_br() {
       return this.dop_br;
    } 
	
    public int getEmergence() {
       return this.emergency;
    } 
	
    public String getSpecific() {
       return this.specific;
    } 
	
	
    public String getStart_date() {
       SimpleDateFormat formatForDateNow = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
       String rDate = formatForDateNow.format(this.start_date);
       return rDate;
    }
	
    public String getEnd_date() throws MyException{
       
       if (this.status != "closed")
       {
           throw new MyException("На вызов о помощи еще не отреагировали!");
       }
		
       SimpleDateFormat formatForDateNow = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
       String rDate = formatForDateNow.format(this.end_date);
       return rDate;
    } 
       
}
