/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;


public class TasksImpl implements Tasks {
    public Map<String, Task> tasks;
    
 public TasksImpl (){
        this.tasks=new HashMap<>();
    }
       
    
       @Override
    public void save(Task pay) {
        tasks.put(pay.getId(), pay);
    }

    @Override
    public Task getById(String id) {
        return tasks.get(id);
    }


    @Override
    public ArrayList<Task> ShowAllTasks () {
        ArrayList ListOfTasks = new ArrayList<>(tasks.entrySet());
        return ListOfTasks;
    }
    
    @Override
    public void UpdateTask(Task newReturn) throws Exception {
     if (getById(newReturn.getId())!=null) {
            tasks.put(newReturn.getId(), newReturn);
        }
        else
            throw new Exception();
    }
    

}
