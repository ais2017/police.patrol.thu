/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

/**
 *
 * @author katerina
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.*;
import java.text.*;
/**
 *
 * @author admin
 */
public class Object {
    private  String address;
    private  String description;
    private  String type;
    public Object(String Address,String Description ,String Type) throws MyException{
       
       if ((Address == null) || ("".equals(Address)))
       {
           throw new MyException("Адрес не может быть пустым!");
       } 
        if ((Type == null) || ("".equals(Type)))
       {
           throw new MyException("Тип не может быть пустым!");
       } 
       this.address = Address;
       this.description = Description;
       this.type = Type;
    }
    
    public void setParam(String Address,String Description ,String Type) throws MyException {
        if ((Address == null) || (Address == ""))
       {
        throw new MyException("Address не может быть пустым!");
       } 
	     if ((Type == null) || (Type == ""))
       {
	throw new MyException("Type не может быть пустым!");
	} 
		  if ((Description == null) || (Description == ""))
       {
			Description = this.description;
		} 
       this.address = Address;
       this.description = Description;
       this.type = Type;
    } 
	
    public String getAddress() {
       return this.address;
    } 
    public String getType() {
       return this.type;
    } 
    public String getDescription() {
       return this.description;
    } 
       
}
