/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;


public class ObjectsImpl implements Objects {
    private AtomicLong currentOrderId = new AtomicLong(0);
    
    public Long incrementAndGetId() {
        return currentOrderId.incrementAndGet();
    }

    public static Map<Long, Object> objects = new HashMap<>();

    
   public void CreateObjectList() throws Exception{
        Object card1 = new Object("333","3232","32323e");
        Object card2 = new Object("333","3232","32323e");
        Object card3 = new Object("333","3232","32323e");
        Object card4 = new Object("333","3232","32323e");
        Object card5 = new Object("333","3232","32323e");
        objects.put(1l, card1);
        objects.put(2l, card2);
        objects.put(3l, card3);
        objects.put(4l, card4);
        objects.put(5l, card5);
    }
    
    @Override
    public void save(Object ret) {
        Long Id = incrementAndGetId();
        objects.put(Id, ret);
    }

    @Override
    public Object getById(Long id) {
        return objects.get(id);
    }

    @Override
    public ArrayList<Object> ShowAllObjects () {
        ArrayList ListOfObjects = new ArrayList<>(objects.entrySet());
        return ListOfObjects;
    }
    
    @Override
    public void UpdateObject(Object newReturn, Long Id) throws Exception {
        if (getById(Id)!=null) {
            objects.put(Id, newReturn);;
        }
        else
            throw new Exception();

    }

}
