/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;
import java.util.*;

    
    public class Scenario {
    public Tasks tasksImpl;
    public Brigades brigadesImpl;
    public Objects objectsImpl;
   // public CallForHelps callforhelpsImpl;
        
    public Scenario (Brigades brs,Tasks tasks,Objects obs){
        tasksImpl = tasks;
        brigadesImpl = brs;
        objectsImpl = obs;
  //      callforhelpsImpl = calls;
    }
    
    public void CreateBrigade(Long Num) throws Exception {
        Brigade new_brigades = brigadesImpl.getById(Num);
        if (new_brigades != null) {
                throw new MyException("Бригада с таким ID уже существует!");
        }
        Brigade br = new Brigade(Num);
        brigadesImpl.save(br);
    }
    
      public void AddTask_with_object_ToBrigade(String idTask,Long Num, String Specific,String Address ,Long object,int code) throws Exception {
        Brigade puch = brigadesImpl.getById(Num);
        if (puch == null)
        {
            throw new MyException("Бригада с таким ID не существует!");
        }
        Object ob = objectsImpl.getById(object);
        if (ob == null)
        {
            throw new MyException("object с таким ID не существует!");
        }
        if (tasksImpl.getById(idTask) != null)
        {
            throw new MyException("tasks с таким ID уже существует!");
        }
        Task a = puch.add_task_with_object(idTask,Specific, Address, ob,code);
        brigadesImpl.UpdateBrigade(puch);
        tasksImpl.save(a);
    }
      
        public void AddTask_with_objects_ToBrigade(String idTask,Long Num, String Specific,String Address ,Long[] object,int code) throws Exception {
        Brigade puch = brigadesImpl.getById(Num);
        if (puch == null)
        {
            throw new MyException("Бригада с таким ID не существует!");
        }
        List<Object> objects = new ArrayList<Object>(0);
        for (int i = 0; i < object.length; i++) {
            Object ob = objectsImpl.getById(object[0]);
            if (ob == null)
        {
            throw new MyException("object с таким ID не существует!");
        }
            objects.add(ob);
        }
              if (tasksImpl.getById(idTask) != null)
        {
            throw new MyException("tasks с таким ID уже существует!");
        }
        Task a = puch.add_task_with_objects(idTask,Specific, Address, objects,code);
        brigadesImpl.UpdateBrigade(puch);
        tasksImpl.save(a);
    }  
      
        public void AddTask_with_address_ToBrigade(String idTask,Long Num, String Specific,String Address ,int code) throws Exception {
        Brigade puch = brigadesImpl.getById(Num);
        if (puch == null)
        {
            throw new MyException("Бригада с таким ID не существует!");
        }
              if (tasksImpl.getById(idTask) != null)
        {
            throw new MyException("tasks с таким ID уже существует!");
        }
        Task a = puch.add_task_with_address(idTask,Specific, Address,code);
        brigadesImpl.UpdateBrigade(puch);
        tasksImpl.save(a);
    }  
   
 
    
    public void addCall_DopBr(Long brN,String idTask,int dop) throws Exception {
        Brigade puch = brigadesImpl.getById(brN);
        if (puch == null)
        {
            throw new MyException("Бригада с таким ID не существует!");
        }
        Task a = tasksImpl.getById(idTask);
        if (a == null)
        {
            throw new MyException("tasks с таким ID не существует!");
        }
        puch.removeTask(a);
        a.addCallForHelp_db(dop);
        tasksImpl.UpdateTask(a);
        puch.addTask(a);
        brigadesImpl.UpdateBrigade(puch);
        tasksImpl.save(a);
    }
        
        public void addCall_Emergency(Long brN,String idTask,int em) throws Exception {
        Brigade puch = brigadesImpl.getById(brN);
        if (puch == null)
        {
            throw new MyException("Бригада с таким ID не существует!");
        }
        Task a = tasksImpl.getById(idTask);
        if (a == null)
        {
            throw new MyException("tasks с таким ID не существует!");
        }
        puch.removeTask(a);
        a.addCallForHelp_emergency(em);
        tasksImpl.UpdateTask(a);
        puch.addTask(a);
        brigadesImpl.UpdateBrigade(puch);
        tasksImpl.save(a);
        }
        
       public void addCallForHelp_S(Long brN,String idTask,String sp) throws Exception {
        Brigade puch = brigadesImpl.getById(brN);
        if (puch == null)
        {
            throw new MyException("Бригада с таким ID не существует!");
        }
        Task a = tasksImpl.getById(idTask);
        if (a == null)
        {
            throw new MyException("tasks с таким ID не существует!");
        }
        puch.removeTask(a);
        a.addCallForHelp_Specific(sp);
        tasksImpl.UpdateTask(a);
        puch.addTask(a);
        brigadesImpl.UpdateBrigade(puch);
        tasksImpl.save(a);
        }
       
       
    public void addReport_to_task(Long brN,String idTask,String cont) throws Exception {
        Brigade puch = brigadesImpl.getById(brN);
        if (puch == null)
        {
            throw new MyException("Бригада с таким ID не существует!");
        }
        Task a = tasksImpl.getById(idTask);
        if (a == null)
        {
            throw new MyException("tasks с таким ID не существует!");
        }
        puch.removeTask(a);
        a.addReport(cont);
        tasksImpl.UpdateTask(a);
        puch.addTask(a);
        brigadesImpl.UpdateBrigade(puch);
        tasksImpl.save(a);
        }
    
}

    

