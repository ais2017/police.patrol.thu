/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

/**
 *
 * @author katerina
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.*;
import java.text.*;
/**
 *
 * @author admin
 */
public class Report {
    private  String content;
    private  Date date;
    public Report(String Content) throws MyException{
       
       if ((Content == null) || (Content == ""))
       {
           throw new MyException("Содержание не может быть пустым!");
       } 
        
       this.date = new Date();
       this.content = Content;
    }
    
    public String getContent() {
       return this.content;
    } 
    public String getDate() {
       SimpleDateFormat formatForDateNow = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
       String rDate = formatForDateNow.format(this.date);
       return rDate;
    } 
       
}

