/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;
import java.util.ArrayList;
/*

public interface Brigades {
    public void save(Brigade brigade);

    public ArrayList<Brigade> ShowAllPurchases();

    public Brigade getById(Long id);
    
    void UpdatePurchase (Brigade newbrigade, Long Id) throws Exception;
    
}*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

public interface Brigades {
    public void save(Brigade br);
    public void CreateBrigadeList () throws Exception;

    public ArrayList<Brigade> ShowAllBrigades();

    public Brigade getById(Long id);
    
    void UpdateBrigade (Brigade newCard) throws Exception;
}

